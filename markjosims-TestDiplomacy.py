from unittest import main, TestCase
from io import StringIO
from Diplomacy import diplomacy_solve

class MyUnitTests(TestCase):
    def test_1(self):
        # several armies attack, only one has support
        r=StringIO("""A Austin Move Llano
                   B Houston Move Llano
                   C ElPaso Move Llano
                   D Dallas Move Llano
                   E SanAntonio Move Llano
                   F Lubbock Move Llano
                   G Llano Hold
                   H Pflugerville Support G""")
        w=StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\n"+
                         "B [dead]\n"+
                         "C [dead]\n"+
                         "D [dead]\n"+
                         "E [dead]\n"+
                        "F [dead]\n"+
                         "G Llano\n"+
                         "H Pflugerville\n")
        
    def test_2(self):
        # army attacked while supporting other army
        # all should die
        r=StringIO("""A Austin Move SanAntonio
                   B SanAntonio Hold
                   C Houston Support A
                   D ElPaso Move Houston""")
        w=StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\n"+
        "B [dead]\n"+
        "C [dead]\n"+
        "D [dead]\n")
        
    def test_3(self):
        # multiple armies battle with various levels of support
        # one army moves and is left alone
        # other holds and is left alone
        r=StringIO("A Austin Hold\n"+
                   "B SanAntonio Move Austin\n"+
                   "C NewBraunfels Support B\n"+
                   "D Dallas Move Austin\n"+
                   "E CedarPark Support A\n"+
                   "F RoundRock Support A\n"+
                   "G Pflugerville Support A\n"+
                   "H Houston Move Austin\n"+
                   "I Katy Support H\n"+
                   "J ElPaso Move Lubbock\n"+
                   "K TexasCity Hold\n")
        w=StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A Austin\n"+
                         "B [dead]\n"+
                         "C NewBraunfels\n"+
                         "D [dead]\n"+
                         "E CedarPark\n"+
                         "F RoundRock\n"+
                         "G Pflugerville\n"+
                         "H [dead]\n"+
                         "I Katy\n"+
                         "J Lubbock\n"+
                         "K TexasCity\n")        

            
if __name__ == '__main__':
    main()