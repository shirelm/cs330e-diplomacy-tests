#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_init, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    test_armies_1 = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n'
    test_armies_2 = 'A Madrid Hold\nB Barcelona Move Madrid\n'
    test_armies_3 = 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n'

    def test_read_1(self):
        s = 'A Madrid Hold\n'
        output = diplomacy_read(s)
        i, j, k = output
        self.assertEqual(i,  'A')
        self.assertEqual(j, 'Madrid')
        self.assertEqual(k, 'Hold')

    def test_read_2(self):
        s = 'B Barcelona Move Madrid\n'
        output = diplomacy_read(s)
        i, j, k, l = output
        self.assertEqual(i,  'B')
        self.assertEqual(j, 'Barcelona')
        self.assertEqual(k, 'Move')
        self.assertEqual(l, 'Madrid')

    def test_read_3(self):
        s = 'C London Support B\n'
        output = diplomacy_read(s)
        i, j, k, l = output
        self.assertEqual(i,  'C')
        self.assertEqual(j, 'London')
        self.assertEqual(k, 'Support')
        self.assertEqual(l, 'B')
    # ----
    # init
    # ----

    def test_init_1(self):
        x = diplomacy_init([diplomacy_read('A Madrid Hold\n')])
        self.assertEqual((x[0]).name, 'A')
        self.assertEqual((x[0]).action, 'Hold')
        self.assertEqual((x[0]).location, 'Madrid')
        self.assertEqual((x[0]).destination, 'Madrid')
        self.assertEqual((x[0]).ally, None)
        self.assertEqual((x[0]).status, 'Madrid')

    
    def test_init_2(self):
        x = diplomacy_init([diplomacy_read('B Barcelona Move Madrid\n')])
        self.assertEqual((x[0]).name, 'B')
        self.assertEqual((x[0]).action, 'Move')
        self.assertEqual((x[0]).location, 'Barcelona')
        self.assertEqual((x[0]).destination, 'Madrid')
        self.assertEqual((x[0]).status, 'Barcelona')
        self.assertEqual((x[0]).ally, None)

    def test_init_3(self):
        x = diplomacy_init([diplomacy_read('C London Support B\n')])
        self.assertEqual((x[0]).name, 'C')
        self.assertEqual((x[0]).action, 'Support')
        self.assertEqual((x[0]).location, 'London')
        self.assertEqual((x[0]).destination, 'London')
        self.assertEqual((x[0]).ally, 'B')
        self.assertEqual((x[0]).status, 'London')

    

    # ----
    # eval
    # ----

    
    def test_eval_1(self):
        r = StringIO(TestDiplomacy.test_armies_1)
        armies = []
        for s in r:
            armies.append(diplomacy_read(s))
        armies = diplomacy_eval(diplomacy_init(armies))
        self.assertEqual(armies[0].status, '[dead]')
        self.assertEqual(armies[1].status, 'Madrid')
        self.assertEqual(armies[2].status, 'London')

    def test_eval_2(self):
        r = StringIO(TestDiplomacy.test_armies_2)
        armies = []
        for s in r:
            armies.append(diplomacy_read(s))
        armies = diplomacy_eval(diplomacy_init(armies))
        self.assertEqual(armies[0].status, '[dead]')
        self.assertEqual(armies[1].status, '[dead]')

    def test_eval_3(self):
        r = StringIO(TestDiplomacy.test_armies_3)
        armies = []
        for s in r:
            armies.append(diplomacy_read(s))
        armies = diplomacy_eval(diplomacy_init(armies))
        self.assertEqual(armies[0].status, '[dead]')
        self.assertEqual(armies[1].status, 'Madrid')
        self.assertEqual(armies[2].status, '[dead]')
        self.assertEqual(armies[3].status, 'Paris')
    

    # -----
    # print
    # -----
    
    def test_print_1(self):
        w = StringIO()
        r = StringIO(TestDiplomacy.test_armies_1)
        armies = []
        for s in r:
            army_info = diplomacy_read(s)
            armies.append(army_info)
        
        diplomacy_print(w, diplomacy_init(armies))
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    def test_print_2(self):
        w = StringIO()
        r = StringIO(TestDiplomacy.test_armies_1)
        armies = []
        for s in r:
            army_info = diplomacy_read(s)
            armies.append(army_info)
        
        diplomacy_print(w, diplomacy_eval(diplomacy_init(armies)))
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        r = StringIO(TestDiplomacy.test_armies_2)
        armies = []
        for s in r:
            army_info = diplomacy_read(s)
            armies.append(army_info)
        
        diplomacy_print(w, diplomacy_eval(diplomacy_init(armies)))
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO(TestDiplomacy.test_armies_1)
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO(TestDiplomacy.test_armies_2)
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO(TestDiplomacy.test_armies_3)
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
...............
----------------------------------------------------------------------
Ran 15 tests in 0.005s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          85      0     52      1    99%   21->23
TestDiplomacy.py     125      0     12      0   100%
--------------------------------------------------------------
TOTAL                210      0     64      1    99%
"""
